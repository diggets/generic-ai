program AITest;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Windows,
  mz.ai.perceptron in '..\lib\mz.ai.perceptron.pas',
  mz.ai.trainer in '..\lib\mz.ai.trainer.pas',
  mz.ai in '..\lib\mz.ai.pas',
  mz.ai.coord in 'mz.ai.coord.pas';

const
  AI_INSTEPS = false;

var LAI    : TCoordAI;
    LLine  : String;
    i      : Integer;
    LErr   : Integer;
    LStart,
    LEnd,
    LFreq  : Int64;
    LTrain : Integer;
    LStep,
    LAbort : Boolean;
begin
  try
    while not LAbort do
    begin
      WriteLn('********************************************************');
      WriteLn('*** Simple Neuronal Network Test ***********************');
      WriteLn('********************************************************');

      LTrain := 0;
      LStep  := false;
      while (LTrain <= 0) do
      begin
        try
          WriteLn('Type in the number of training (f.e. "100"):');
          ReadLn(LLine);
          if not TryStrToInt(LLine, LTrain) then
            raise EAbort.Create('invalid numeric value!');

          if (LTrain < 0) then
            raise EAbort.Create('only positive numeric values allowed!');

          if (LTrain > 10000000) then
            raise EAbort.Create('value to large for this test case!');
        except
          on e:Exception do
          begin
            WriteLn(e.Message);
          end;
        end;
      end;

      LAI := TCoordAI.Create(LTrain);
      try
        while true do
        begin
          WriteLn('********************************************************');
          WriteLn('*** OPTIONS ********************************************');
          WriteLn('*** ENTER - execute learning process');
          WriteLn('*** "exit" - quit program');
          WriteLn('*** "reset" - restart program');
          WriteLn('*** "step" - pause each 1000 steps');
          WriteLn('********************************************************');
          WriteLn('');
          ReadLn(LLine);

          if (LLine.ToLower() = 'exit') then
          begin
            LAbort := true;
            break;
          end;

          if (LLine.ToLower() = 'reset') then
          begin
            LAbort := false;
            break;
          end;

          if (LLine.ToLower() = 'step') then
          begin
            LStep := AI_INSTEPS;
          end;

          QueryPerformanceCounter(LStart);
          LErr := LTrain;
          i := 0;
          while (LErr > 0) and ((not LStep) or (LStep and (i < 1000))) do
          begin
            LErr := LAI.Execute();
            inc(i);
          end;
          QueryPerformanceCounter(LEnd);
          QueryPerformanceFrequency(LFreq);

          WriteLn(Format('estimated time: %n sec.', [(LEnd-LStart)/LFreq]));
          WriteLn('');
        end;
      finally
        FreeAndNil(LAI);
      end;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
