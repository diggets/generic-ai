(*
 *  Copyright 2020 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)
 
unit mz.ai.coord;

interface

uses
  System.Types,
  System.Classes,
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections,
  mz.ai,
  mz.ai.perceptron,
  mz.ai.trainer;

type
  TCoordPerceptron = class(TPerceptron<Double, Integer, Double>)
    protected
      ///
      /// handling weight callbacks
      ///
      procedure SetWeight(AIndex : Integer; const AValue : Double); override;
      function DoOnCreateWeight() : Double; override;
      function DoOnWeightToString(const AWeight : Double) : String; override;
      procedure DoOnNotifyWeight(ASender : TObject; const AWeight : Double;
        AAction : TCollectionNotification); override;

      ///
      /// training callbacks
      ///
      function DoOnCalculateError(const ADesired, AGuess : Integer) : Integer; override;
      function DoOnRealignWeight(const AWeight : Double; const AError : Integer;
        const AInput : Double) : Double; override;

      ///
      /// feed forward callbacks
      ///
      procedure DoOnPrepareSummarize(var ASum : Double); override;
      function DoOnSummarize(const ASum : Double; const AWeight : Double;
        const AInput : Double) : Double; override;
      function DoOnActivate(const ASum : Double) : Integer; override;

    public
  end;

  TCoordTrainer = class(TTrainer<Double, Integer>)
    protected
      function GetBiasValue() : Double; override;

    public
      function ToString() : String; override;
  end;

  ///
  /// A simple AI class implementation for usage of trainer and perceptron
  ///
  TCoordAI = class(TAI<TCoordPerceptron, TCoordTrainer, Integer>)
    protected
      ///
      /// creation callbacks
      ///
      function DoOnCreatePerceptron() : TCoordPerceptron; override;
      function DoOnCreateTrainer() : TCoordTrainer; override;
      procedure DoOnNotifyTrainer(ASender : TObject; const ATrainer : TCoordTrainer;
        AAction : TCollectionNotification); override;

      ///
      /// execution callbacks
      ///
      procedure DoOnTrain(const ATrainer : TCoordTrainer); override;
      function DoOnFeedForward(const ATrainer : TCoordTrainer) : Integer; override;
      function DoOnPerceptronToString() : String; override;

      ///
      /// result evaluation callbacks
      ///
      function DoOnCompareGuessAndTrainer(const AGuess : Integer;
        const ATrainer : TCoordTrainer) : Boolean; override;
      procedure DoOnGuess(const ATrainer : TCoordTrainer; const AGuess : Integer;
        const ACorrect : Boolean); override;

    public
      constructor Create(const ATraining : Integer); virtual;
      destructor Destroy(); override;
  end;

implementation


uses
  System.Math;

const
  /// Coordinate space
  XMIN : Integer = -400;
  YMIN : Integer = -100;
  XMAX : Integer =  400;
  YMAX : Integer =  100;

{ TCoordTrainer }
function TCoordTrainer.GetBiasValue() : Double;
begin
  result := 1;
end;

function TCoordTrainer.ToString() : String;
begin
  result := ''; Format('(%n, %n)[%d]', [FInputs[0], FInputs[1], FAnswer]);
end;

{ TCoordPerceptron }

procedure TCoordPerceptron.SetWeight(AIndex : Integer; const AValue : Double);
begin
  inherited SetWeight(AIndex, AValue);

  FWeights[AIndex] := AValue;
end;

function TCoordPerceptron.DoOnCreateWeight() : Double;
const
  WEIGHT_RANDOM_SEED = 1000;

var
  LMidSeed,
  LSeed   : Integer;
begin
  LMidSeed := WEIGHT_RANDOM_SEED div 2;
  LSeed := WEIGHT_RANDOM_SEED + 1;

  if (Random(WEIGHT_RANDOM_SEED) < LMidSeed) then
    result := 0.001 * Random(LSeed)
  else
    result := -0.001 * Random(LSeed);
end;

function TCoordPerceptron.DoOnCalculateError(const ADesired, AGuess : Integer) : Integer;
begin
  result := ADesired - AGuess;
end;

function TCoordPerceptron.DoOnRealignWeight(const AWeight : Double; const AError : Integer;
  const AInput : Double) : Double;
begin
  result := AWeight + Self.FLearningRate * AError * AInput;
end;

function TCoordPerceptron.DoOnWeightToString(const AWeight : Double) : String;
begin
  result := FloatToStr(AWeight);
end;

procedure TCoordPerceptron.DoOnNotifyWeight(ASender : TObject; const AWeight : Double;
  AAction : TCollectionNotification);
begin
  case AAction of
    cnAdded: ;
    cnRemoved: ;
    cnExtracted: ;
  end;
end;

procedure TCoordPerceptron.DoOnPrepareSummarize(var ASum : Double);
begin
  ASum := 0;
end;

function TCoordPerceptron.DoOnSummarize(const ASum : Double; const AWeight : Double; const AInput : Double) : Double;
begin
  result := ASum + AWeight * AInput;
end;

function TCoordPerceptron.DoOnActivate(const ASum : Double) : Integer;
begin
  if (ASum > 0) then result := 1
                else result := -1;
end;

{ TCoordAI}

constructor TCoordAI.Create(const ATraining : Integer);
begin
  inherited Create(ATraining);
end;

destructor TCoordAI.Destroy();
begin
  FreeAndNil(FPerceptron);
  FreeAndNil(FTrainer);

  inherited Destroy();
end;

function TCoordAI.DoOnCreatePerceptron() : TCoordPerceptron;
begin
  result := TCoordPerceptron.Create(3, 0.00001);
end;

function TCoordAI.DoOnCreateTrainer() : TCoordTrainer;
var LX, LY   : Double;
    LAnswer  : Integer;

  function f(x : Double) : Double;
  begin
    result := 0.4 * x + 1;
  end;

begin
  LX := RandomRange(XMIN, XMAX);
  LY := RandomRange(YMIN, YMAX);

  if (LY < f(LX)) then LAnswer := -1
  else LAnswer := 1;

  result := TCoordTrainer.Create(LX, LY, LAnswer);
end;

procedure TCoordAI.DoOnNotifyTrainer(ASender : TObject; const ATrainer : TCoordTrainer;
  AAction : TCollectionNotification);
begin
  case AAction of
    cnAdded: ;
    cnRemoved:
      begin
        ATrainer.Free();
      end;

    cnExtracted: ;
  end;
end;

procedure TCoordAI.DoOnTrain(const ATrainer : TCoordTrainer);
begin
  FPerceptron.Train(ATrainer.Inputs, ATrainer.Answer);
end;

function TCoordAI.DoOnFeedForward(const ATrainer : TCoordTrainer) : Integer;
begin
  result := FPerceptron.FeedForward(ATrainer.Inputs);
end;

function TCoordAI.DoOnPerceptronToString() : String;
begin
  result := FPerceptron.ToString();
end;

function TCoordAI.DoOnCompareGuessAndTrainer(const AGuess : Integer;
  const ATrainer : TCoordTrainer) : Boolean;
begin
  result := AGuess = ATrainer.Answer;
end;

procedure TCoordAI.DoOnGuess(const ATrainer : TCoordTrainer; const AGuess : Integer; const ACorrect : Boolean);
begin
  //
end;


end.
