(*
 *  Copyright 2020 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)
 
unit mz.ai.perceptron;

interface

uses
  System.Types,
  System.Classes,
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections;

type
  EIllegalArgumentException = class(Exception)
  end;

  TWeight<T> = class
    protected
      FValue : T;

      function GetValue() : T; virtual; abstract;
      procedure SetValue(const AValue : T); virtual; abstract;

    public
      property Value : T read GetValue write SetValue;

      constructor Create(); virtual;

      procedure Assign(const ASource : TWeight<T>); virtual;
      function Multiply(const AInput : T) : T; virtual; abstract;
      function ToString() : String; override;
  end;

  ///
  /// See: http://en.wikipedia.org/wiki/Perceptron
  ///
  TPerceptron<TInputType, TOutputType, TWeightType> = class
    protected
      /// Array of weights for inputs
      FWeights : TList<TWeightType>;
      /// learning constant
      FLearningRate : Double;

      ///
      /// weight handling methods
      ///
      function GetWeight(AIndex : Integer) : TWeightType; virtual;
      procedure SetWeight(AIndex : Integer; const AValue : TWeightType); virtual;
      function DoOnCreateWeight() : TWeightType; virtual; abstract;
      procedure DoOnNotifyWeight(ASender : TObject; const AWeight : TWeightType;
        AAction : TCollectionNotification); virtual; abstract;
      function DoOnWeightToString(const AWeight : TWeightType) : String; virtual; abstract;

      ///
      /// training callbacks
      ///
      function DoOnCalculateError(const ADesired, AGuess : TOutputType) : TOutputType; virtual; abstract;
      function DoOnRealignWeight(const AWeight : TWeightType; const AError : TOutputType;
        const AInput : TInputType) : TWeightType; virtual; abstract;

      ///
      /// feed forward callbacks
      ///
      procedure DoOnPrepareSummarize(var ASum : TInputType); virtual; abstract;
      function DoOnSummarize(const ASum : TInputType; const AWeight : TWeightType; const AInput : TInputType) : TInputType; virtual; abstract;
      function DoOnActivate(const ASum : TInputType) : TOutputType; virtual; abstract;

    public
      property Weights[Index : Integer] : TWeightType read GetWeight write SetWeight;

      ///
      /// Perceptron is created with n weights and learning constant
      ///
      constructor Create(const AWeightsCount : Integer; const ALearningRate : Double); virtual;

      destructor Destroy(); override;

      ///
      /// Function to train the Perceptron
      /// Weights are adjusted based on desired answer
      ///
      procedure Train(const AInputs : TArray<TInputType>; const ADesired : TOutputType); virtual;

      ///
      /// Guess -1 or 1 based on input values
      ///
      function FeedForward(const AInputs : TArray<TInputType>) : TOutputType; virtual;

      ///
      /// output perceptron data as string
      ///
      function ToString() : String; override;
  end;

implementation

{ TWeight<T> }

constructor TWeight<T>.Create();
begin
  inherited Create();
end;

procedure TWeight<T>.Assign(const ASource : TWeight<T>);
begin
  FValue := ASource.FValue;
end;

function TWeight<T>.ToString() : String;
begin
  result := '?';
end;

{ TPerceptron<TInputType, TOutputType, TWeightType> }

constructor TPerceptron<TInputType, TOutputType, TWeightType>.Create(
  const AWeightsCount : Integer; const ALearningRate : Double);
var i       : Integer;
    LWeight : TWeightType;
begin
  inherited Create();

  FLearningRate := ALearningRate;

  FWeights := TList<TWeightType>.Create();
  FWeights.OnNotify := DoOnNotifyWeight;
  for i := 0 to (AWeightsCount - 1) do
  begin
    LWeight := DoOnCreateWeight();
    FWeights.Add(LWeight);
  end;
end;

destructor TPerceptron<TInputType, TOutputType, TWeightType>.Destroy();
var i : Integer;
begin
  FreeAndNil(FWeights);

  inherited Destroy();
end;

function TPerceptron<TInputType, TOutputType, TWeightType>.GetWeight(AIndex: Integer): TWeightType;
begin
  if (AIndex < 0) or (AIndex >= FWeights.Count) then
    raise EIllegalArgumentException.Create('invalid weight index');

  result := FWeights.Items[AIndex];
end;

procedure TPerceptron<TInputType, TOutputType, TWeightType>.SetWeight(AIndex : Integer; const AValue : TWeightType);
begin
  if (AIndex < 0) or (AIndex >= FWeights.Count) then
    raise EIllegalArgumentException.Create('invalid weight index');
end;

procedure TPerceptron<TInputType, TOutputType, TWeightType>.Train(const AInputs : TArray<TInputType>; const ADesired : TOutputType);
var LGuess : TOutputType;
    LError : TOutputType;
    i : Integer;
begin
  /// Guess the result
  LGuess := FeedForward(AInputs);

  /// Compute the factor for changing the weight based on the error
  /// Error = desired output - guessed output
  /// Note this can only be 0, -2, or 2
  /// Multiply by learning constant
  LError := DoOnCalculateError(ADesired, LGuess);

  /// Adjust weights based on weightChange  input
  for i := 0 to (FWeights.Count - 1) do
  begin
    FWeights.Items[i] := DoOnRealignWeight(FWeights.Items[i], LError, AInputs[i])
  end;
end;

function TPerceptron<TInputType, TOutputType, TWeightType>.FeedForward(const AInputs : TArray<TInputType>) : TOutputType;
var LSum : TInputType;
    i : Integer;
begin
  /// Sum all values
  DoOnPrepareSummarize(LSum);
  for i := 0 to (FWeights.Count - 1) do
  begin
    LSum := DoOnSummarize(LSum, FWeights.Items[i], AInputs[i]);
  end;

  /// Result is sign of the sum, -1 or 1
  result := DoOnActivate(LSum);
end;

function TPerceptron<TInputType, TOutputType, TWeightType>.ToString() : String;
var LBldr : TStringBuilder;
    i : Integer;
begin
  LBldr := TStringBuilder.Create();
  try
    LBldr.Append('[');
    for i := 0 to (FWeights.Count - 1) do
    begin
      if (i > 0) then
        LBldr.Append(', ');

      LBldr.Append(
        DoOnWeightToString(FWeights.Items[i]));
    end;
    LBldr.Append(']');
    LBldr.Append(#10);
    LBldr.Append(FLearningRate);
  finally
    result := LBldr.ToString();
    FreeAndNil(LBldr);
  end;
end;

end.
