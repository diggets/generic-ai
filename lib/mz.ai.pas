(*
 *  Copyright 2020 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)

unit mz.ai;

interface

uses
  System.Types,
  System.Classes,
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections,
  mz.ai.perceptron,
  mz.ai.trainer;

type
  ///
  /// A simple AI class implementation for usage of trainer and perceptron
  ///
  TAI<TPerceptronType, TTrainerType, TOutputType> = class
    protected
      /// A Perceptron object
      FPerceptron : TPerceptronType;
      /// A list of points we will use to "train" the perceptron
      FTrainer : TList<TTrainerType>;
      /// error count for this execution cycle
      FErrors : Integer;
      /// count of execution cycles
      FIterations : Integer;

      ///
      /// AI creation / destruction callbacks
      ///
      function DoOnCreatePerceptron() : TPerceptronType; virtual; abstract;
      function DoOnCreateTrainer() : TTrainerType; virtual; abstract;
      procedure DoOnNotifyTrainer(ASender : TObject; const ATrainer : TTrainerType;
        AAction : TCollectionNotification); virtual; abstract;

      ///
      /// AI execution callbacks
      ///
      procedure DoOnTrain(const ATrainer : TTrainerType); virtual; abstract;
      function DoOnFeedForward(const ATrainer : TTrainerType) : TOutputType; virtual; abstract;
      function DoOnPerceptronToString() : String; virtual; abstract;

      ///
      /// AI result evaluation callbacks
      ///
      function DoOnCompareGuessAndTrainer(const AGuess : TOutputType;
        const ATrainer : TTrainerType) : Boolean; virtual; abstract;
      procedure DoOnGuess(const ATrainer : TTrainerType; const AGuess : TOutputType;
        const ACorrect : Boolean); virtual; abstract;

    public
      constructor Create(const ATraining : Integer); virtual;
      destructor Destroy(); override;

      function Execute() : Integer; virtual;
  end;

implementation

uses
  System.Math;

{ TAI<TPerceptronType, TTrainerType> }

constructor TAI<TPerceptronType, TTrainerType, TOutputType>.Create(const ATraining : Integer);
var i        : Integer;
    LTrainer : TTrainerType;
begin
  inherited Create();

  // The perceptron has 3 inputs -- x, y, and bias
  // Second value is "Learning Constant"
  // Learning Constant is low just b/c it's fun to watch, this is not necessarily optimal
  FPerceptron := DoOnCreatePerceptron();

  // Create a random set of training points and calculate the "known" answer
  FTrainer := TList<TTrainerType>.Create();
  FTrainer.OnNotify := DoOnNotifyTrainer;
  for i := 0 to (ATraining - 1) do
  begin
    LTrainer := DoOnCreateTrainer();
    FTrainer.Add(LTrainer);
  end;
end;

destructor TAI<TPerceptronType, TTrainerType, TOutputType>.Destroy();
begin
  FreeAndNil(FPerceptron);
  FreeAndNil(FTrainer);

  inherited Destroy();
end;

function TAI<TPerceptronType, TTrainerType, TOutputType>.Execute() : Integer;
var i, j   : Integer;
    LGuess : TOutputType;
begin
  inc(FIterations);
  FErrors := 0;

  for j := 0 to (FTrainer.Count - 1) do
  begin
    // Train the Perceptron with one "training" point at a time
    DoOnTrain(FTrainer[j]);
  end;

  // Draw all the points based on what the Perceptron would "guess"
  // Does not use the "known" correct answer
  for i := 0 to (j - 1) do
  begin
    LGuess := DoOnFeedForward(FTrainer[i]);
    if not DoOnCompareGuessAndTrainer(LGuess, FTrainer[i]) then
    begin
      inc(FErrors);
      DoOnGuess(FTrainer[i], LGuess, false);
    end
    else DoOnGuess(FTrainer[i], LGuess, true);
  end;

  WriteLn(Format('=> execution-cycle #%d, errors = %d', [FIterations, FErrors]));
  WriteLn(DoOnPerceptronToString());
  result := FErrors;
end;

end.
