(*
 *  Copyright 2020 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)
 
unit mz.ai.trainer;

interface

uses
  System.Types,
  System.Classes,
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections;

type
  ///
  /// A class to describe a training point
  /// Has an x and y, a "bias" (1) and known output
  /// Could also add a variable for "guess" but not required here
  ///
  TTrainer<TInputType, TOutputType> = class
    protected
      FInputs : TArray<TInputType>;
      FAnswer : TOutputType;

      function GetBiasValue() : TInputType; virtual; abstract;

    public
      property Inputs : TArray<TInputType> read FInputs;
      property Answer : TOutputType read FAnswer;

      constructor Create(const X : TInputType; const Y : TInputType; const AAnswer : TOutputType); virtual;

      function ToString() : String; override;
  end;

implementation

constructor TTrainer<TInputType, TOutputType>.Create(const X : TInputType; const Y : TInputType; const AAnswer : TOutputType);
begin
  inherited Create();

  System.SetLength(FInputs, 3);
  FInputs[0] := X;
  FInputs[1] := Y;
  FInputs[2] := GetBiasValue(); /// bias

  FAnswer := AAnswer;
end;

function TTrainer<TInputType, TOutputType>.ToString() : String;
begin
  result := '';
end;

end.
